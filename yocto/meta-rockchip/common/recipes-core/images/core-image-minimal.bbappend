IMAGE_INSTALL_append += " kernel-modules"

# Strip all extra spaces in rootfs
python () {
    d.delVar('IMAGE_ROOTFS_EXTRA_SPACE')
    d.setVar('IMAGE_ROOTFS_EXTRA_SPACE', '0')
}

# For ap6255 wifi
IMAGE_INSTALL_append += " linux-firmware-bcm43455"
IMAGE_INSTALL_append += " brcm-tools"

# For wifi test
IMAGE_INSTALL_append += " iw wireless-tools tcpdump"

# For codec test
IMAGE_INSTALL_append += " alsa-utils"

# For wifi setup(softap)
IMAGE_INSTALL_append += " hostapd dnsmasq wpa-supplicant"
